<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php if(is_search()) : ?>
            <title>Searching results - PrimeVisionGroup</title>
        <?php elseif(is_front_page()) : ?>
            <title>PrimeVisionGroup</title>
        <?php else: ?>
            <title><?= wp_get_document_title('|', true, 'right');?></title>
        <?php endif; ?>
<!--    <title>--><?php //wp_title( '|', true, 'right' );  ?><!--</title>-->
    <?php wp_head(); ?>

</head>
<body>
<!--START HEADER-->
<header class="wrapper">
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="phone">
                        <?php $home_page = get_page_by_title( 'Home' ); ?>
                        <?php
                        //get theme options
                        $options = get_option( 'theme_settings' ); ?>
<!--                        <li><a href="#"><span>Тel.:</span>  --><?php //echo get_option('company_phone'); ?><!--</a></li>-->
                        <li><a href="mailto: <?php echo get_option('company_email'); ?>"><span>E-mail:</span> <?php echo get_option('company_email'); ?></a></li>
                    </ul>
                    <div class="search">
                        <form action="<?php bloginfo( 'url' ); ?>" method="get">
                            <input  type="search" name="s" placeholder="Search" value="<?php if(!empty($_GET['s'])){echo $_GET['s'];}?>"/>
<!--                        <input type="search">-->
                        <i class="icon-research-loop"></i>
                        </form>
                    </div>
                    <ul class="reg">
                        <li id="subscribe_for_free_button"><a href="<?= get_home_url(); ?>"><i class="icon-subscribe"></i> Subscribe Now</a></li>
<!--                        <li>--><?php //echo do_shortcode("[tw_user_login]"); ?><!--</li>-->
<!--                        <li>--><?php //echo do_shortcode("[fb_user_login]"); ?><!--</li>-->
<!--                        --><?php //if (is_user_logged_in()) : ?>
<!--                            <li><a href="--><?php //echo wp_logout_url( home_url() ); ?><!--" class="logout">Log out</a></li>-->
<!--                        --><?php //else : ?>
<!--                            <li><a href="http://primevisiongroup.com/wp-login.php?loginTwitter=1&redirect=http://primevisiongroup.com" onclick="window.location = 'http://primevisiongroup.com/wp-login.php?loginTwitter=1&redirect='+window.location.href; return false;"><i class="fa fa-twitter"></i></a></li>-->
<!--                            <li><a href="http://prime-vision-group.alscon-clients.com/wp-login.php?loginFacebook=1&redirect=http://prime-vision-group.alscon-clients.com" onclick="window.location = 'http://prime-vision-group.alscon-clients.com/wp-login.php?loginFacebook=1&redirect='+window.location.href; return false;"><i class="fa fa-facebook"></i></a></li>-->
<!--                            <li><a href="#" class="sign-in">Sign in</a></li>-->
<!--                        --><?php //endif;  ?>

    <!--                        <li><a href="#" class="sign-in">Sign in</a> / </li>-->
<!--                        <li><a href="#" class="register">Register</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo">
<!--                        <a href="/"><img src="--><?php //var_dump( get_option('company_logo')); die(); ?><!--" alt=""></a>-->
                        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_field('company_logo',$home_page->ID); ?>" alt=""></a>
                    </div>
                    <div class="menu">
                        <i class="fa fa-bars"></i>
                        <?php echo pvg_full_menu_setup('top_main'); ?>
<!--                        --><?php //do_action('facebook_login_button');?>
<!--                        <ul>-->
<!--                            <li class="active"><a href="index.html"><i class="icon-Home"></i>Home</a></li>-->
<!--                            <li><a href="about-us.html"><i class="icon-about"></i>About Us</a></li>-->
<!--                            <li><a href="#"><i class="icon-research"></i>Reports Research</a></li>-->
<!--                            <li><a href="contact.html"><i class="icon-contact"></i>Contact</a></li>-->
<!--                        </ul>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--END HEADER-->