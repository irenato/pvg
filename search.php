<?php
/*
Template Name: Search
*/

get_header(); ?>

    <!--START CONTENT-->
    <section class="wrapper banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                    <img src="<?=get_template_directory_uri()?>/images/banner-Research.png" alt="">
                    <div class="container">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="banner-text">
                                <h2 class="title"><span class="sline">Searching</span> results</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="content research all">
                        <h3><span class="sline">Latest Rese</span>arch Reports</h3>

                        <?php
                            $args = array_merge( $wp_query->query, array( 'post_type' => array("post", "latest-research-reports")) );
                            query_posts($args); ?>
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt=""></a>
                            <h4><?php the_title(); ?></h4>
                            <p><?php the_content(); ?></p>
                            <p class="data"><b><?php echo get_the_date('m.d.Y');?></b> by <?php the_author();?></p>
<!--                            <p class="data"><b>--><?php //echo get_the_date('F j, Y');?><!--</b> by --><?php //the_author();?><!--</p>-->
                            <ul>
<!--                                <li>-->
<!--                                    <a href="#">Leave a comment</a>-->
<!--                                </li>-->
                                <li>
                                    <a href="<?php the_permalink(); ?>">Continue Reading</a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <hr class="sline">

                            <?php endwhile; else: ?>
                                <p>The search has not given any results.</p>
                            <?php endif;?>
                      </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!--END CONTENT-->

<?php get_footer(); ?>