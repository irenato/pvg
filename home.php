<?php
/*
Template Name: Home
*/
get_header();

?>

<section class="wrapper slider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                <img src="<?=get_template_directory_uri()?>/images/slider.png" alt="">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-text">
                            <?php $page = get_page_by_title( 'Home' ); ?>
                            <h2 class="title"><?= get_post_meta($page->ID, 'main_page_title_part1', true) ?> <br><span class="sline"><?= get_post_meta($page->ID, 'main_page_title_part2', true) ?></span></h2>
                            <h3 class="title-min"><?= get_post_meta($page->ID, 'main_page_title_small', true) ?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-12">
                <div class="content">
                    <h3><span class="sline">Latest Res</span>earch Reports</h3>
                    <?php

                    get_template_part( 'featured-content');
                    ?>

                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
                <div class="right-sitebar">
<!--                    --><?php //echo do_shortcode("[mc4wp_form id=\"54\"]"); ?>
                    <?php

                    get_template_part( 'smartresponder_right_form');
                    ?>
<!--                    <h3>Sign up to receive news <span class="sline">and repor</span>ts</h3>-->
<!--                    <input type="search" placeholder="Please Enter Your Email Address">-->
<!--                    <input type="submit" value="Sign up">-->
                    <h5>PrimeVisionGroup</h5>
                    <h3><span class="sline">about </span>us</h3>
                    <div class="description">
                        <?php $page = get_page_by_title( 'Home' ); ?>
                        <img src="<?php echo get_field('home_about_us_image',$page->ID); ?>" alt="">
                        <p><?php echo get_post_meta($page->ID, 'home_about_us', true); ?></p>
                        <?php $page_about_us = get_page_by_title( 'About us' );?>
                        <a href="<?= $page_about_us->guid ?>">Continue Reading</a>
                    </div>
<!--                    <h3><span class="sline">Follow U</span>s</h3>-->
<!--                    <a href="--><?//= get_option('twitter_link') ?><!--" class="button-soc tw">-->
<!--                        <i class="fa fa-twitter"></i>follow IN <b>TWITTER</b>-->
<!--                    </a>-->
<!--                    --><?php //$facebook_link = get_option('facebook_link'); ?>
<!--                    --><?php //if(!empty($facebook_link)) : ?>
<!--                        <a href="--><?php //echo $facebook_link; ?><!--" class="button-soc fc">-->
<!--                            <i class="fa fa-facebook"></i>read IN <b>FACEBOOK</b>-->
<!--                        </a>-->
<!--                    --><?php //endif; ?>
<!--                    <h3><span class="sline">ARCHIVES</span></h3>-->
<!--                    <p class="select">-->
<!--                        <select name="#" id="#">-->
<!--                            --><?php //wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
<!--<!--                            <option value="#">Select  month</option>-->
<!--<!--                            <option value="#">Select  month</option>-->
<!--<!--                            <option value="#">Select  month</option>-->
<!--                        </select>-->
<!--                        <i class="selects"></i>-->
<!--                    </p>-->
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>