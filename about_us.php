<?php
/*
Template Name: About us
*/

get_header(); ?>

<section class="wrapper banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                <img src="<?=get_template_directory_uri()?>/images/banner-about.png" alt="">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="banner-text">
                            <h2 class="title"><span class="sline">About </span>us</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-11 col-xs-12 ">
                <div class="content about">
                    <h3><span class="sline">Who is pri</span>me vision group</h3>
                    <h4><?php echo get_post_meta($post->ID, 'our_target_title', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'our_target', true); ?></p>
                    <h4><?php echo get_post_meta($post->ID, 'what_we_do_title', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'what_we_do', true); ?></p>
                    <h4><?php echo get_post_meta($post->ID, 'quality_title', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'quality', true); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
