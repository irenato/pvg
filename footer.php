<!--START FOOTER-->
<?php //addNewUser($_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['password']); ?>
<?php //if(isset($_GET['auth'])) {
//	activateUserByToken($_GET['auth']);
//}?>

<footer class="wrapper">
	<div class="top-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="logo">
						<?php $home_page = get_page_by_title( 'Home' ); ?>
						<a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_field('company_logo',$home_page->ID); ?>" alt=""></a>
					</div>
					<p class="text"><?php echo (get_option('footer_description')); ?> </p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<h3>address</h3>
					<ul>
<!--						<li><a href="#">Tel.: --><?php //echo get_option('company_phone'); ?><!--</a></li>-->
						<li><a class='footer-email' href="mailto: <?php echo get_option('company_email'); ?>"><span>E-mail: </span><?php echo get_option('company_email'); ?></a></li>
					</ul>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
					<h3>Follow us</h3>
					<ul>
						<li><a href="<?php if (!empty(get_option('twitter_link'))) {echo get_option('twitter_link');}else{echo'#';} ?>"><i class="fa fa-twitter"></i></a></li>
						<?php if (!empty(get_option('facebook_link'))) {echo '<li><a href="get_option(\'facebook_link\')"><i class="fa fa-facebook"></i></a></li>';} ?>
					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
<!--					--><?php //echo do_shortcode("[mc4wp_form id=\"54\"]"); ?>
					<h3>Join our Email List</h3>
					<!-- SmartResponder.ru subscribe form code (begin) -->
					<?php
						get_template_part( 'smartresponder_footer_form');
					?>
					<!-- SmartResponder.ru subscribe form code (end) -->

<!--					<input type="text" placeholder="Enter Your Email Address">-->
<!--					<input type="submit" value="SIGN UP">-->
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php echo pvg_full_bottom_menu_setup('bottom_main'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<p>Copyright © 2016 PrimeVisionGroup. All rights reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--END FOOTER-->


<!--START MODAL-->
<div class="modal" id="reg">
	<div class="block-modal">
		<h3 class="title"><span class="sline">REGISTR</span>ATION</h3>
		<form action="">
			<label for="">First name <input type="text" name="first_name"></label>
			<label for="">Last name <input type="text"  name="last_name"></label>
			<label for="">Email Address <input type="email" name="email"></label>
			<label for="">Password <input type="password" name="password"></label>
			<label for="">Confirm Password <input type="password"  name="password_again"></label>
			<input type="submit" id="register-button" class="button" value="Join now">
			<div class="error-message" style="display: none">
				<p style="color: #761c19"></p>
			</div>
		</form>
		<span class="close">X</span>
	</div>
</div>



<div class="modal" id="sign">
	<div class="block-modal sign">
		<h3 class="title"><span class="sline">sign in </span>to your account</h3>
		<form action="">
			<label for="">Email Address <input type="text" name="user-login"></label>
			<label for="">Password <input type="password" name="user-password"></label>

			<input type="submit" id="sign-in-button" class="button" value="Join now">
			<p class="checkbox">
				<input id="checkbox" type="checkbox">
				<label for="checkbox">Remember me</label>
			</p>
			<span class="new-user">New user? <a href="#">Join Now!</a></span>
			<span> <a href="<?php echo wp_lostpassword_url( get_permalink() ); ?>">Forgot password?</a></span>

			<div class="error-message-login" style="display: none">
				<p style="color: #761c19"></p>
			</div>
		</form>
		<span class="close">X</span>
	</div>
</div>
<!--START MODAL-->

<!-- SmartResponder.ru subscribe form code (begin) -->
<?php
	get_template_part( 'smartresponder_modal');
?>
<!-- SmartResponder.ru subscribe form code (end) -->
<?php wp_footer(); ?>
</body>
</html>