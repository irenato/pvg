$(document).ready(function(){
    var error_message;
    $("input#register-button").click(function(e){
        e.preventDefault();
        var first_name, last_name, email, password, password_again;

        first_name = $("input[name='first_name']").val();
        last_name = $("input[name='last_name']").val();
        email = $("input[name='email']").val();
        password = $("input[name='password']").val();
        password_again = $("input[name='password_again' ]").val();
        if(last_name == '' || first_name == '' || email == '' || password == '' || password_again == ''){
            error_message = 'Fill in all the fields';
        }else if(password.length<8){
            error_message = 'Minimum password length of 8 characters';
        }else if(password != password_again){
            error_message = 'Passwords do not match';
        }

        if(error_message && error_message.length>0){
            $("div.error-message p").text(error_message);
            $("div.error-message").show();
            //alert(error_message);
        }else{
            var user_data = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'user_complete_registration',
                    'first_name': first_name,
                    'email' : email,
                    'last_name': last_name,
                    'password': password
                },
            });
            user_data.done(function(data){
                if(data == 'Sorry, that username already exists!'){
                    error_message = 'Sorry, that email already exists!';
                    $("div.error-message p").text(error_message);
                    $("div.error-message").show();
                    //alert('Sorry, that email already exists!')
                }else{
                    location.reload();
                }
            })
        }
    })

    $("span.new-user").click(function(e){
        e.preventDefault();
        $('#sign').hide();
        $('#reg').show();
    })

    $("input#sign-in-button").click(function(e){
        e.preventDefault();
        var user_login, user_password, user_remember;
        user_login = $("input[name='user-login']").val();
        user_password = $("input[name='user-password']").val();
        user_remember = $("input#checkbox").prop('checked');
        if(user_login == '' || user_password == ''){
            error_message = 'Fill in all the fields';
            $("div.error-message-login p").text(error_message);
            $("div.error-message-login").show();
        }else{
            var user_data_login = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'users_login',
                    'user_login': user_login,
                    'user_password' : user_password,
                    'user_remember' : user_remember
                },
            });
            user_data_login.done(function(data){
                if(data != 0){
                    error_message = 'Incorrect login or password';
                    $("div.error-message-login p").text(error_message);
                    $("div.error-message-login").show();
                }else{
                    location.reload();
                }
            })
        }
    })
///contact us. validation and send
    $("input#send_mail").click(function(e){
        e.preventDefault();
        $("div.error-message-contact-us p").text('');
        var user_name, user_email, mail_subject, mail_text;

        user_name = $("input[name='user_name']").val();
        user_email = $("input[name='user_email']").val();
        mail_subject = $("input[name='mail_subject']").val();
        mail_text = $("textarea[name='mail_text']").val();

        if(user_name == '' || user_email == '' || mail_subject == '' || mail_text == ''){
            error_message_contact_us = 'Fill in all the fields';
            $("div.error-message-contact-us p").text(error_message_contact_us);
            $("div.error-message-contact-us").show();
        }else{
            var contact_us = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'contact_us',
                    'user_name': user_name,
                    'user_email': user_email,
                    'mail_subject': mail_subject,
                    'mail_text': mail_text
                },
            });
            contact_us.done(function(data){
                if(data == 'done!'){
                    user_name = $("input[name='user_name']").val('');
                    user_email = $("input[name='user_email']").val('');
                    mail_subject = $("input[name='mail_subject']").val('');
                    mail_text = $("textarea[name='mail_text']").val('');
                    error_message_contact_us = 'Your email was successfully sent.';
                    $("div.error-message-contact-us p").text(error_message_contact_us);
                    $("div.error-message-contact-us").show();
                }else{
                    error_message_contact_us = 'Your email has not been sent.';
                    $("div.error-message-contact-us p").text(error_message_contact_us);
                    $("div.error-message-contact-us").show();
                }
            })
        }
    })

    $("a#single-comment-button").click(function(e){
        e.preventDefault();
        $("div.comment-form-block").slideToggle();
    })

    //redirect click to plugin link
    //$("a.click-to-subscribe").click(function(e){
    //    e.preventDefault();
    //    var skrill = $(this).closest("div.choose-period").find("input[type='submit']");
    //    //console.log(skrill);
    //    skrill.trigger( "click" );
    //})

    $("a.click-to-subscribe").click(function(e){
        e.preventDefault();
        var amount, subscribe_to_period, description;
        amount = $(this).closest("div.choose-period").find("span.amount-value").text();
        subscribe_to_period = $(this).closest("div.choose-period").find("h4").text();
        description  = $(this).closest("div.choose-period").find("p.period-description").text();
        var send_data  = $.ajax({
            url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                'action': 'my_simple_skrill_payment',
                'subscribe_to_period': subscribe_to_period,
                'description' : description,
                'amount' : amount
            },
        });


    })

    $("#subscribe_for_free_button").click(function(e){
        e.preventDefault();
        $("#subscibe_for_free").show();
        $('body').css('overflow', 'hidden');
    })

    $('.close').click(function() {
        $('#subscibe_for_free').hide();
        $('body').css('overflow', 'auto');
    })

    $(document).mouseup(function (e)  {
        var folder = $("#subscibe_for_free ");

        if (!folder.is(e.target) && folder.has(e.target).length === 0) {
            folder.hide();
            $('body').css('overflow', 'auto');
        }
    });

//challenge model by clicking on the menu item lower "Subscribe"
    if($('ul.footer-menu li a i').hasClass('trial')){
        var trial = $('i.trial').parent();
    }

    trial.click(function(e){
        e.preventDefault();
        $("#subscibe_for_free").show();
        $('body').css('overflow', 'hidden');
    })
})