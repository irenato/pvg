<?php
/*
Template Name: Privacy Policy
*/
get_header(); ?>
<section class="wrapper banner">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
            <img src="<?=get_template_directory_uri()?>/images/banner-privacy.png" alt="">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="banner-text">
<!--                        <h3 class="title-min">Small capitalized company</h3>-->
                        <h2 class="title"><span class="sline">Privacy </span>Policy</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-11 col-xs-12 ">
                <div class="content privacy">
<!--                    <h3><span class="sline">Privacy Po</span>licy</h3>-->
                    <p class="it"><?php echo get_post_meta($post->ID, 'privacy_policy', true); ?></p>
                    <h4><?php echo get_post_meta($post->ID, 'privacy_policy_2', true); ?></h4>
                    <ul>
                        <li><?php echo get_post_meta($post->ID, 'privacy_policy_3', true); ?></li>
                        <li><?php echo get_post_meta($post->ID, 'privacy_policy_4', true); ?></li>
                        <li><?php echo get_post_meta($post->ID, 'privacy_policy_5', true); ?></li>
                    </ul>
                    <h4><?php echo get_post_meta($post->ID, 'privacy_policy_6', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'privacy_policy_7', true); ?></p>
                    <ul>
                        <li><?php echo get_post_meta($post->ID, 'privacy_policy_8', true); ?></li>
                        <li><?php echo get_post_meta($post->ID, 'privacy_policy_9', true); ?></li>
                        <li><?php echo get_post_meta($post->ID, 'privacy_policy_10', true); ?></li>
                        <li><?php echo get_post_meta($post->ID, 'privacy_policy_11', true); ?></li>
                    </ul>
                    <h4><?php echo get_post_meta($post->ID, 'privacy_policy_12', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'privacy_policy_13', true); ?></p>
                    <h4><?php echo get_post_meta($post->ID, 'privacy_policy_14', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'privacy_policy_15', true); ?></p>
                    <h4><?php echo get_post_meta($post->ID, 'privacy_policy_16', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'privacy_policy_17', true); ?></p>
                    <h4><?php echo get_post_meta($post->ID, 'privacy_policy_18', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'privacy_policy_19', true); ?></p>
                    <h4><?php echo get_post_meta($post->ID, 'privacy_policy_20', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'privacy_policy_21', true); ?></p>
                    <h4><?php echo get_post_meta($post->ID, 'privacy_policy_22', true); ?></h4>
                    <p><?php echo get_post_meta($post->ID, 'privacy_policy_23', true); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--END CONTENT-->

<?php get_footer(); ?>
