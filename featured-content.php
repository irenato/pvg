<?php
/**
 * Created by PhpStorm.
 * User: Alscon13
 * Date: 08.04.2016
 * Time: 10:11
 */
?>
<?php
$paged = get_query_var('paged') ? get_query_var('paged') : 1;
$args = array(
        'category_name' => 'latest-research-reports',
        'paged' => $paged,
        'post_status' => 'publish',
        'posts_per_page' => 5,
        'caller_get_posts'=> 1);?>
<?php //$args = array('posts_per_page' => 2,
//                    'category_name' => 'latest-research-reports',
//                    'paged' => $paged); ?>

<?php $wp_query  = new WP_query($args);?>

<?php while ( $wp_query ->have_posts() ) : $wp_query ->the_post(); ?>

    <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt=""></a>
    <h4><a class="title"  href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
    <p class="text"><?php the_content(); ?></p>
    <p class="data"><b><?php echo get_the_date('m.d.Y');?></b> by <?php the_author();?></p>
    <!--                            <p class="data"><b>--><?php //echo get_the_date('F j, Y');?><!--</b> by --><?php //the_author();?><!--</p>-->
    <ul>
        <!--                                <li>-->
        <!--                                    <a href="--><?php //the_permalink(); ?><!--#comments_field">Leave a comment</a>-->
        <!--                                </li>-->
        <li>
            <a href="<?php the_permalink(); ?>">Continue Reading</a>
        </li>
    </ul>
    <div class="clearfix"></div>
    <hr class="sline">


    <!--    <a class="title" href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--        <p class="text">--><?php //the_content(); ?><!--</p>-->
<!--        <p class="data"><b>--><?php //echo get_the_date('m.d.Y');?><!--</b> by --><?php //the_author();?><!--</p>-->
<!--        <ul>-->
<!---->
<!--            <li>-->
<!--                <a href="--><?php //echo get_field('pdf_file',$post->ID); ?><!--">Continue Reading</a>-->
<!--            </li>-->
<!--        </ul>-->
<!--        <hr class="sline">-->
<?php endwhile; ?>
<div class="pagination">
<?php if(function_exists('tw_pagination'))
    tw_pagination();
?>
</div>
