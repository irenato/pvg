<?php
/**
 * Created by PhpStorm.
 * User: Alscon13
 * Date: 07.04.2016
 * Time: 16:34
 */
add_theme_support('post-thumbnails');

function pvg_style()
{
    wp_enqueue_style('try-new-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', '', '', 'all');
    wp_enqueue_style('try-new-bootstrap-grid-3.3.1.min', get_template_directory_uri() . '/css/bootstrap-grid-3.3.1.min.css', '', '', 'all');
    wp_enqueue_style('try-new-bootstrap-theme.min', get_template_directory_uri() . '/css/bootstrap-theme.min.css', '', '', 'all');
    wp_enqueue_style('try-new-font-awesome.min', get_template_directory_uri() . '/css/font-awesome.min.css', '', '', 'all');
    wp_enqueue_style('try-new-ico', get_template_directory_uri() . '/css/ico.css', '', '', 'all');
    wp_enqueue_style('try-new-main', get_template_directory_uri() . '/css/main.css', '', '', 'all');
    wp_enqueue_style('try-new-media', get_template_directory_uri() . '/css/media.css', '', '', 'all');
}

add_action('wp_enqueue_scripts', 'pvg_style');

function pvg_scripts()
{
    wp_enqueue_script('try-new-jquery-2.1.4.min', get_template_directory_uri() . '/js/jquery-2.1.4.min.js', false, '', true);
    wp_enqueue_script('try-new-bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', false, '', true);
    wp_enqueue_script('try-new-common', get_template_directory_uri() . '/js/common.js', false, '', true);
    wp_enqueue_script('try-new-action', get_template_directory_uri() . '/js/action.js', false, '', true);
    wp_localize_script('try-new-action', 'ajax_object', array(
        'ajax_url' => admin_url('admin-ajax.php')
    ));
}

add_action('wp_enqueue_scripts', 'pvg_scripts');

if (!function_exists('pvg_theme_setup')) :

    function pvg_theme_setup()
    {

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'top_main' => __('Top Menu'),
            'bottom_main' => __('Bottom Menu')
        ));

        function my_wp_nav_menu_args($args = '')
        {
            $args['container'] = '';

            return $args;
        }

        add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args');


    }
endif;
add_action('after_setup_theme', 'pvg_theme_setup');

function pvg_full_menu_setup($menu_name = 'top_main')
{
    $options = get_option('theme_settings');
    $menu_list = "";
    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
        global $post;
        $page_for_posts = get_option('page_for_posts');

        if ($page_for_posts) {
            if (is_home()) {
                $thePostID = $page_for_posts;
            } else {
                $thePostID = $post->ID;
            }
        } else {
            $thePostID = $post->ID;
        }

        $menu = wp_get_nav_menu_object($locations[$menu_name]);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        //var_dump($menu_items);
        //die;

        $menu_list = '<ul>';
        $submenu = false;
        $parent_id = 0;
        $count = 0;
        $ico_array = array('icon-Home', 'icon-about', 'icon-research', 'icon-contact');
        $i = -1;
        foreach ($menu_items as $menu_item) {
            $i++;
            $title = $menu_item->title;
            $url = $menu_item->url;
            $active_class = $thePostID == $menu_item->object_id ? "active" : "";
            if (!$menu_item->menu_item_parent) {

                $menu_list .= ' <li class="' . $active_class . '"><a href="' . $url . '"><i class="' . $ico_array[$i] . '"></i><i class="top-menu-text">' . $title . '</i></a></li>';

            }

            $count++;
        }
        $menu_list .= "</ul>";
    }
    return $menu_list;
}


add_action('after_setup_theme', 'pvg_theme_setup');

function pvg_full_bottom_menu_setup($menu_name = 'bottom_main')
{
    $options = get_option('theme_settings');
    $menu_list = "";
    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
        global $post;
        $page_for_posts = get_option('page_for_posts');

        if ($page_for_posts) {
            if (is_home()) {
                $thePostID = $page_for_posts;
            } else {
                $thePostID = $post->ID;
            }
        } else {
            $thePostID = $post->ID;
        }

        $menu = wp_get_nav_menu_object($locations[$menu_name]);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        //var_dump($menu_items);
        //die;

        $menu_list = '<ul class="footer-menu">';
        $submenu = false;
        $parent_id = 0;
        if (get_option('trial_period') == true) {
            $for_trial = 'trial';
        } else {
            $for_trial = '';
        }
        $count = 0;
        $ico_array = array('icon-Home', 'icon-about', 'icon-research', 'icon-subscribe ' . $for_trial, 'icon-contact', 'icon-privasy', 'icon-disclaimer');
        $i = -1;

        foreach ((array)$menu_items as $menu_item) {
            $i++;
            $title = $menu_item->title;
            $url = $menu_item->url;

            $active_class = $thePostID == $menu_item->object_id ? "active" : "";
//            if(get_page_link()  == $url){
//                $active_class = 'active';
//            }else{
//                $active_class = '';
//            }

            if (!$menu_item->menu_item_parent) {

                // save this id for later comparison with sub-menu items

                $menu_list .= ' <li class="' . $active_class . '"><a href="' . $url . '"><i class="' . $ico_array[$i] . '"></i>' . $title . '</a></li>';

            }

            $count++;
        }
        $menu_list .= "</ul>";
    }
    return $menu_list;
}


function check_user()
{
    $email = stripcslashes(trim($_POST['email']));
//    $check_email = email_exists();
    if ($user = get_user_by('email', $email)) {
        echo 2;
    } else {
        echo 1;
    }

    die();
}

add_action('wp_ajax_nopriv_check_user', 'check_user');
add_action('wp_ajax_check_user', 'check_user');

function user_complete_registration()
{

    $first_name = stripcslashes(trim($_POST['first_name']));
    $last_name = stripcslashes(trim($_POST['last_name']));
    $email = stripcslashes(trim($_POST['email']));
    $password = stripcslashes(trim($_POST['password']));
    $userdata = array(
        'user_login' => $email,
        'user_email' => $email,
        'user_pass' => $password,
        'first_name' => $first_name,
        'last_name' => $last_name,
    );
    $user_id = wp_insert_user($userdata);
    if (!is_integer($user_id)) {
        print_r($user_id->get_error_message());
//        return 2;
        die();
    } else {
//        return false;
        print_r($user_id);
        die();
    }


}

add_action('wp_ajax_nopriv_user_complete_registration', 'user_complete_registration');
add_action('wp_ajax_user_complete_registration', 'user_complete_registration');

function users_login()
{
    $user_login = stripcslashes(trim($_POST['user_login']));
    $user_password = stripcslashes(trim($_POST['user_password']));

    $creds = array();
    $creds['user_login'] = $user_login;
    $creds['user_password'] = $user_password;
    $creds['remember'] = $_POST['user_remember'];

    $user = wp_signon($creds, false);

    if (is_wp_error($user)) {
        echo $user->get_error_message();
        die();
    }
}

add_action('wp_ajax_nopriv_users_login', 'users_login');
add_action('wp_ajax_users_login', 'users_login');

function contact_us()
{
    $user_name = $_POST['user_name'];
    $user_email = $_POST['user_email'];
    $subject = $_POST['mail_subject'];
    $message = $_POST['mail_text'];
    $to = 'primevisiongroup.com@gmail.com';
//    $to = get_option('contact_email');
    $headers[] = 'From: ' . $user_name . ' < ' . $user_email . ' >';
    $headers[] = 'MIME-Version: 1.0' . "\r\n";
    $headers[] = 'Content-type: text/html; charset=UTF-8';
    if ( wp_mail($to, $subject, $message, $headers)) {
        print('done!');
        die();
    } else {
        print('error!');
        die();
    }
}

add_action('wp_ajax_nopriv_contact_us', 'contact_us');
add_action('wp_ajax_contact_us', 'contact_us');


//Disable admin bar for all users except Adminisatratot(admin)
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar()
{
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

//тестовый вариант!!! email плательщика и получателя перепутаны!!!
function my_simple_skrill_payment()
{
    global $current_user;
    get_currentuserinfo();
    $email = get_option('skrill_login');
    $password = get_option('skrill_password');
    $currency = 'USD';
    $amount = $_POST['amount'];
    $bnf_email = $current_user->user_email;
    $subject = str_replace(" ", "-", $_POST['subscribe_to_period']);

    $note = $_POST['description'];
    $reference_id = rand(000000, 999999);
    $send_data_for_prepare = 'https://www.skrill.com/app/pay.pl?action=prepare&email=' . $email . '&password=' . $password . '&amount=' . $amount . '&currency=' . $currency . '&bnf_email=' . $bnf_email . '&subject=' . $subject . '&note=dsd&frn_trn_id=' . $reference_id;
    $result_prepare = file_get_contents($send_data_for_prepare);
    $response = new SimpleXMLElement($result_prepare);
    if (isset($response->error)) {
        echo($response->error->error_msg);
        die();
    } else {
        $transfer_path = 'https://www.skrill.com/app/pay.pl?action=transfer&sid=' . $response->sid;
        $result_transfer = file_get_contents($transfer_path);
        $response_transfer = new SimpleXMLElement($result_transfer);
        if ($response_transfer->transaction->status_msg == 'processed') {
            echo('done');
            if ($subject == '1-YEAR-SUBSCRIPTION' && $response_transfer->transaction->amount == $amount) {
                $user_id = wp_update_user(array('user_email' => $current_user->user_email, 'user_status' => 3));
                echo('done');
                die();
            } else if ($subject == '3-month-SUBSCRIPTION' && $response_transfer->transaction->amount == $amount) {
                $user_id = wp_update_user(array('user_email' => $current_user->user_email, 'user_status' => 2));
                echo('done');
                die();
            } else {
                echo('error');
                die();
            }

        }
    }
    die();
}

add_action('wp_ajax_nopriv_my_simple_skrill_payment', 'my_simple_skrill_payment');
add_action('wp_ajax_my_simple_skrill_payment', 'my_simple_skrill_payment');


function themeslug_theme_customizer($wp_customize)
{
    $wp_customize->add_section('themeslug_logo_section', array(
        'title' => __('Logo', 'themeslug'),
        'priority' => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));

    $wp_customize->add_setting('themeslug_logo');
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'themeslug_logo', array(
        'label' => __('Logo', 'themeslug'),
        'section' => 'themeslug_logo_section',
        'settings' => 'themeslug_logo',
    )));
}

add_action('customize_register', 'themeslug_theme_customizer');






