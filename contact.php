<?php
/*
Template Name: Contact
*/

get_header(); ?>

<section class="wrapper banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                <img src="<?=get_template_directory_uri()?>/images/banner-contact.png" alt="">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="banner-text">
<!--                            <h3 class="title-min">Small capitalized company</h3>-->
                            <h2 class="title"><span class="sline">Contac</span>t</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-11 col-xs-12 ">
                <div class="content contact">
                    <h3><span class="sline">Send a message and we will contact you soon</span></h3>
                    <?= do_shortcode('[contact-form-7 id="244" title="PrimeVisionGroupContactForm"]'); ?>
<!--                    <form action="">-->
<!--                        <label for="">Your Name</label>-->
<!--                        <input type="text" name="user_name">-->
<!--                        <label for="">Your Email</label>-->
<!--                        <input type="email" name="user_email">-->
<!--                        <label for="">Message subject</label>-->
<!--                        <input type="text" name="mail_subject">-->
<!--                        <label for="">Your Message</label>-->
<!--                        <textarea name="mail_text" id="#"></textarea>-->
<!--                        <input id="send_mail" type="submit" value="send">-->
<!--                        <div class="error-message-contact-us" style="display: none">-->
<!--                            <p style="color: #761c19"></p>-->
<!--                        </div>-->
<!--                    </form>-->
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
