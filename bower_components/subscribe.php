<?php
/*
Template Name: Subscribe
*/

get_header(); ?>

<section class="wrapper banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                <img src="<?=get_template_directory_uri()?>/images/banner-subscribe.png" alt="#">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="banner-text">
                            <h3 class="title-min">Small capitalized company</h3>
                            <h2 class="title"><span class="sline">Subscri</span> be Now</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-11 col-xs-12 ">
                <div class="content subscribe">
                    <h3><span class="sline">Subscribe</span> Now to The PRIME VISION GROUP Report!</h3>
                    <h4>1-YEAR SUBSCRIPTION</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.</p>
                    <p><i class="icon-money"></i>Value: 3$</p>
                    <a href="#" class="button">Subscribe</a>
                    <h4>3-month SUBSCRIPTION:</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.</p>
                    <p><i class="icon-money"></i>Value: 1$</p>
                    <a href="#" class="button">Subscribe</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>

