<?php
/**
 * Template name: PDF news
 */
require_once get_theme_root().'/prime-vision-group/lib/fpdf/fpdf.php';
$post_id = $_GET['id'];
$content = get_post_field('post_content', $post_id);

$images = array();
$content_texts = array();
preg_match_all('/< *img[^>]*src *= *["\']?([^"\']*)/i', $content, $images, PREG_OFFSET_CAPTURE);
$offset_point = 0;
if(isset($images[0])) {
    for($i = 0; $i <= count($images[0]); $i++) {
        if($i == count( $images[0] )) {
            $content_texts[] = strip_tags( substr( $content, $offset_point, strlen($content) ) );
        }
        else {
            $content_texts[] = strip_tags( substr( $content, $offset_point, $images[0][$i][1] ) );
        }
        if( isset( $images[0][$i][1] ) )$offset_point = $images[0][$i][1];
    }
}

$title = get_post_field('post_title', $post_id);
$pdf = new FPDF();
$pdf->AddFont( 'OpenSans','','42f21395b80d09e9b09982e978b1da33_opensans-regular.php' );
$pdf->SetFont('OpenSans','',14);
$pdf->AddPage();
$pdf->Image(get_template_directory_uri().'/img/logo-1.png', 10, 10);
$pdf->ln(7);
$pdf->Cell(100, 5, '');
$pdf->Cell(50, 5, 'Print');
$pdf->ln(14);
$pdf->Cell(30, 5, '');
$pdf->SetFont('OpenSans','',18);
$pdf->MultiCell(130, 6, custom_enc( $title ) );
$pdf->ln(10);
$pdf->SetFont('OpenSans','',10);
for($i = 0; $i < count($content_texts); $i++) {
    $pdf->MultiCell(180, 5, custom_enc( $content_texts[$i] ) );
    $pdf->ln(5);
    if( isset( $images[1][$i][0] ) && $images[1][$i][0] ) {
        $height_match = array();
        $width_match = array();
        $imagesize = getimagesize($images[1][$i][0]);
        preg_match('/width="([^"]*)"/', $imagesize[3], $width_match);
        preg_match('/height="([^"]*)"/', $imagesize[3], $height_match);
        $height = 0;
        $width = 0;
        if( isset( $width_match[1] ) ) {
            $width = $width_match[1];
        }
        if( isset( $height_match[1] ) ) {
            $height = $height_match[1];
        }
        $pdf->Cell( 40, 40, $pdf->Image($images[1][$i][0], $pdf->GetX(), $pdf->GetY(), 0, 0), 0, 0, 'L', false );
        $pdf->ln($height);
    }
    $pdf->ln(5);

}
$pdf->ln(5);
$pdf->Cell(40, 5, get_the_permalink($post_id),  0, 0, 'L', false, get_the_permalink($post_id)  );
$pdf->ln(5);
$pdf->Cell( 40, 40, 'PrimeVisionGroup', 0, 0, 'L', false, 'http://primevisiongroup.com/' );
$pdf->ln(5);
$pdf->Cell( 40, 40, date('d.m.Y') );
$pdf->ln(5);
$pdf->Output();
?>