<?php
/*
Template Name: Disclaimer
*/

get_header(); ?>

<!--START CONTENT-->
<section class="wrapper banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                <img src="<?=get_template_directory_uri()?>/images/banner-disclamer.png" alt="">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="banner-text">
<!--                            <h3 class="title-min">Small capitalized company</h3>-->
                            <h2 class="title"><span class="sline">Disclaim</span>er</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-11 col-xs-12 ">
                <div class="content disclaimer">
                    <h3><span class="sline">Disclaime</span>r</h3>
                    <p><strong><?php echo get_post_meta($post->ID, 'disclaimer_text', true); ?></strong></p>
                    <p><?php echo get_post_meta($post->ID, 'disclaimer_text_2', true); ?></p>
                    <p><?php echo get_post_meta($post->ID, 'disclaimer_text_3', true); ?></p>
                    <p><?php echo get_post_meta($post->ID, 'disclaimer_text_4', true); ?></p>
                    <p><?php echo get_post_meta($post->ID, 'disclaimer_text_5', true); ?></p>
                    <p><?php echo get_post_meta($post->ID, 'disclaimer_text_6', true); ?></p>
                    <p><?php echo get_post_meta($post->ID, 'disclaimer_text_7', true); ?></p>
                    <p><?php echo get_post_meta($post->ID, 'disclaimer_text_8', true); ?></p>
                    <p><strong><?php echo get_post_meta($post->ID, 'disclaimer_text_9', true); ?></strong></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--END CONTENT-->

<?php get_footer(); ?>
