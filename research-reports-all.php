<?php
/*
Template Name: Research reports all
*/

get_header(); ?>

<!--START CONTENT-->
<section class="wrapper banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                <img src="<?=get_template_directory_uri()?>/images/banner-Research.png" alt="">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="banner-text">
<!--                            <h3 class="title-min">Small capitalized company</h3>-->
                            <h2 class="title"><span class="sline">Reports</span> research</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 ">
                <div class="content research all">
                    <h3><span class="sline">Latest Rese</span>arch Reports</h3>
                    <?php $paged = get_query_var('paged') ? get_query_var('paged') : 1; ?>
                    <?php $args = array('category_name' => 'latest-research-reports',
                        'paged' => $paged,
                        'post_status' => 'publish',
                        'posts_per_page' => 5,
                        'caller_get_posts'=> 1); ?>

                    <?php $wp_query = new WP_query($args);?>

                    <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                        <a href="<?php the_permalink(); ?>"><img class="single-img-research-reports" src="<?php the_post_thumbnail_url(); ?>" alt=""></a>
                        <div class="clearfix"></div>
                        <h4><a class="title"  href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        <div class="content-research-reports">
                            <p class="text"><?php the_content(); ?></p>
                        </div>
                        <p class="data"><b><?php echo get_the_date('m.d.Y');?></b> by <?php the_author();?></p>
                        <!--                            <p class="data"><b>--><?php //echo get_the_date('F j, Y');?><!--</b> by --><?php //the_author();?><!--</p>-->
                        <ul>
                            <!--                                <li>-->
                            <!--                                    <a href="--><?php //the_permalink(); ?><!--#comments_field">Leave a comment</a>-->
                            <!--                                </li>-->
                            <li>
                                <a href="<?php the_permalink(); ?>">Continue Reading</a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                        <hr class="sline">

                    <?php endwhile; ?>
                    </div>
                <div class="pagination">
                    <?php if(function_exists('tw_pagination'))
                        tw_pagination();
                    ?>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--END CONTENT-->

<?php get_footer(); ?>



