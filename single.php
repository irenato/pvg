<?php
/*
Template Name: Single
*/

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<section class="wrapper banner">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
				<img src="<?=get_template_directory_uri()?>/images/banner-Research.png" alt="">
				<div class="container">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="banner-text">
<!--							<h3 class="title-min">Small capitalized company</h3>-->
							<h2 class="title"><span class="sline">Reports</span> research</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 ">
				<div class="content research">
					<?php $title = strtoupper(get_the_title()); ?>
					<h2 class="title"><span class="sline"><?php echo substr($title, 0, 3); ?></span><?php echo substr($title, 3, (strlen($title)-1)); ?></h2>
<!--					<h2>--><?php //the_title(); ?><!--</h2>-->
					<p><img class="post-img" src="<?php the_post_thumbnail_url($post->ID, 'large'); ?>" alt=""></p>
					<div class="clearfix"></div>
					<div class="single-content">
						<p><?php the_content(''); ?></p>
					</div>
					<p></p>
					<a id="comments_field"></a>
					<p class="data"><b><?php echo get_the_date('m.d.Y');?></b> by <?php the_author();?></p>
<!--					Комментарии временно отключены -->
<!--					<div class="comment-form-block" style="display: none">-->
<!---->
<!--						--><?php //comments_template(); ?>
<!--					</div>-->
<!--					Комментарии временно отключены -->
					<ul>
						<li>
<!--							--><?php //if (is_user_logged_in()) : ?>
<!--								<a href="" id="single-comment-button">Leave a comment</a>-->
<!--							--><?php //else : ?>
<!--								<a href="#" class="sign-in">Leave a comment</a>-->
<!--							--><?php //endif;  ?>

							<a href="<?php echo get_field('pdf_file',$post->ID); ?>">Continue Reading</a>
						</li>
						<li>
							<i class="other"><?php next_post_link('%link', 'other research', true); ?></i>
<!--							<a href="--><?php //get_next_post_link(); ?><!--" class="other">other research</a>-->
						</li>
					</ul>
					<hr class="sline">

					<h5>Download full version in PDF</h5>

					<a href="<?php echo get_field('pdf_file',$post->ID); ?>" class="button" download>DOWNLOAD</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endwhile;?>
<?php endif;?>
<?php get_footer(); ?>
