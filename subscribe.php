<?php
/*
Template Name: Subscribe
*/

get_header(); ?>

<section class="wrapper banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                <img src="<?=get_template_directory_uri()?>/images/banner-subscribe.png" alt="#">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="banner-text">
<!--                            <h3 class="title-min">Small capitalized company</h3>-->
                            <h2 class="title"><span class="sline">Subscri</span>be Now</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-11 col-xs-12 ">
                <div class="content subscribe">
                    <h3><span class="sline">Subscribe</span> Now to The PRIME VISION GROUP Report!</h3>
                    <div class="choose-period">
                        <h4><?php echo get_post_meta($post->ID, 'year', true); ?></h4>
                        <p class="period-description"><?php echo get_post_meta($post->ID, 'year_description', true); ?></p>
                        <p><i class="icon-money"></i>Value: <span class="amount-value"><?php echo get_post_meta($post->ID, 'year_price', true); ?></span>$</p>
                        <a href="#" class="button click-to-subscribe">Subscribe </a>

<!--                        <div class="payment-skrill" style="display: none">--><?php //echo do_shortcode("[skrill_simple amount=".get_post_meta($post->ID, 'year_price', true)." label=".get_post_meta($post->ID, 'year', true)." description=".get_post_meta($post->ID, 'year_description', true)." ] "); ?><!--</div>-->
                    </div>
                    <div class="choose-period">
                        <h4><?php echo get_post_meta($post->ID, 'month', true); ?></h4>
                        <p class="period-description"><?php echo get_post_meta($post->ID, 'month_description', true); ?></p>
                        <p><i class="icon-money"></i> Value: <span class="amount-value"><?php echo get_post_meta($post->ID, 'month_price', true); ?></span>$</p>
                        <a href="#" class="button click-to-subscribe">Subscribe </a>
<!--                        <div class="payment-skrill" style="display: none">--><?php //echo do_shortcode("[skrill_simple amount=".get_post_meta($post->ID, 'month_price', true)." label=".get_post_meta($post->ID, 'month', true)." description=".get_post_meta($post->ID, 'month_description', true)." ] "); ?><!--</div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>

